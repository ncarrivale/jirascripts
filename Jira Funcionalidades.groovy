******************************************************************************************
COMENTAR UN ISSUE
******************************************************************************************

import com.atlassian.jira.ComponentManager
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.comments.CommentManager

compManager = ComponentManager.getInstance()

def issueManager = ComponentAccessor.getIssueManager()
def currentUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
def user = currentUser.getName()
MutableIssue issue = issueManager.getIssueObject("PHP-16")
def ida = issue.getKey()

CommentManager commentMgr = compManager.getCommentManager()
commentMgr = (CommentManager) compManager.getComponentInstanceOfType(CommentManager.class)
commentMgr.create(issue, user, ida, false)


******************************************************************************************
LOGUEAR LINEA
******************************************************************************************

import org.apache.log4j.Logger
import org.apache.log4j.Level
log.setLevel(Level.DEBUG)

log.debug(LOQUEQUIERO)


******************************************************************************************
UPDATEAR VALOR DE UN CAMPO
******************************************************************************************

import com.atlassian.jira.issue.Issue
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.ComponentManager
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.ModifiedValue
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder

//Identify Issue
def im = ComponentAccessor.getIssueManager()
def issue = im.getIssueObject("PHP-13")
def cfm = ComponentAccessor.getCustomFieldManager()

//Set Change Holder
def changeHolder = new DefaultIssueChangeHolder();

//Set Value of Manufactuer
def cf1 = cfm.getCustomFieldObjects(issue).find {it.name == "Valor"}
cf1.updateValue(null, issue, new ModifiedValue(issue.getCustomFieldValue(cf1), 'TEST_VALUE'), changeHolder);

******************************************************************************************
MOSTRAR EXCEPTION PARA DEBUGGEAR EN SCRIPT CONSOLE
******************************************************************************************

import com.opensymphony.workflow.InvalidInputException
throw new InvalidInputException ("CLARO")

******************************************************************************************
ENVIAR MAIL 
******************************************************************************************

import com.atlassian.mail.Email
import com.atlassian.mail.server.MailServerManager
import com.atlassian.mail.server.SMTPMailServer
import com.atlassian.jira.component.ComponentAccessor
//import com.atlassian.jira.functest.framework.admin

def subject = "test"
def body = "test"
//def emailAddr = issue.getReporter().getEmailAddress()
def emailAddr = "nicolas.carrivale@tsoftlatam.com"
 
def sendEmail(emailAddr, subject, body)
{
def mailServer = ComponentAccessor.getMailServerManager().getDefaultSMTPMailServer()
    
    if (mailServer) 
    {
        Email email = new Email(emailAddr);
        email.setSubject(subject);
        email.setBody(body);
        mailServer.send(email);
	}
    
    else
    {
        // Problem getting the mail server from JIRA configuration, log this error
    }
}

sendEmail (emailAddr, subject, body)

******************************************************************************************
VALIDACION - AL MENOS UN ATTACH
******************************************************************************************

import com.atlassian.jira.component.ComponentAccessor
ComponentAccessor.attachmentManager.getAttachments(issue).size() >= 1

******************************************************************************************
UPDATEAR VALOR DE CAMPOS POR API REST
******************************************************************************************

def issueKey = issue.key
// get custom fields
def customFields = get("/rest/api/2/field")
    .asObject(List)
    .body
    .findAll { (it as Map).custom } as List<Map>

// Tomo el issue
def issue = get("/rest/api/2/issue/${issueKey}") //${issueKey}
    .header('Content-Type', 'application/json')
    .asObject(Map)
    .body

//Obtengo todos los ids de los campos
def orgAplicCf = customFields.find { it.name == 'Organismo de aplicacion' }?.id
def dirOmicCf = customFields.find { it.name == 'Direccion OMIC' }?.id
def contactoOrgCf = customFields.find { it.name == 'Contacto Org.' }?.id
def mailOrgCf = customFields.find { it.name == 'Email Org.' }?.id
def telOrgCf = customFields.find { it.name == 'Telefono Org.' }?.id
def CoprecCf = customFields.find { it.name == 'COPREC' }?.id

// Get all the fields from the issue as a Map
def fields = issue.fields as Map

def provinciaCiudadCf = customFields.find { it.name == 'Provincia / Ciudad' }?.id
def valueCfProvincia = fields[provinciaCiudadCf] as Map //Con este valor haremos el if para calcular el estudio
def valueCoprecCf = fields[CoprecCf] as Map

// Get each of the values from the multi select list field and store them
def ciudadSeleccionadaMap = valueCfProvincia?.child as Map
def ciudadSeleccionada = ciudadSeleccionadaMap?.value
def provinciaSeleccionada = valueCfProvincia?.value
def coprecSeleccionadaMap = valueCoprecCf?.child as Map
def COPREC = coprecSeleccionadaMap?.value

def orgAplicValue = ""
def dirOmicValue = ""
def contactoOrgValue = ""
def mailOrgValue = ""
def telOrgValue = ""

// Descarto OMIC si COPREC = Si y Provincia = "Capital Federal" (VALIDAR *********)

if (COPREC != "No" && provinciaSeleccionada != "Capital Federal") {

//En base a la provincia defino la Omic
// Casos por Provincia y dentro Casos por Ciudad
// OJO CABA que tienen todos GCP tienen CP 1000 **************************

    switch(provinciaSeleccionada){
          case "Catamarca":
            switch(ciudadSeleccionada){
                case "San Fernando Del Valle De Catamarca (4700)":
                    orgAplicValue = "Defensa del Consumidor"
                    dirOmicValue = "Avenida General Belgrano Nro 1494-Pabell�n 27"
                    contactoOrgValue = ""
                    telOrgValue = ""
                    mailOrgValue = ""
                    break
                case "San Jose (Dpto. Santa Maria) (4139)":
                    orgAplicValue = "Oficina de Defensa al Consumidor Munic. San Jose"
                    dirOmicValue = "Calle 16 de Agosto s/n"
                    contactoOrgValue = ""
                    telOrgValue = ""
                    mailOrgValue = ""
                    break
                default:
                    orgAplicValue = ""
                    break
            }

case "Chaco":
            switch(ciudadSeleccionada){
                case "Barranqueras (3503)":
                    orgAplicValue = "Municipalidad de Barranqueras- Area Defensa del Consumidor"
                    dirOmicValue = "Avenida Laprida 5601"
                    contactoOrgValue = ""
                    telOrgValue = "0362-4581058"
                    mailOrgValue = "defensadelconsumidor@barranqueras.gob.ar"
                    break
                case "Fontana (3514)":
                    orgAplicValue = "Ministerio de Industria, Empleo y Trabajo - Secretaria de Comercio"
                    dirOmicValue = "9 de Julio"
                    contactoOrgValue = ""
                    telOrgValue = "3624475857"
                    mailOrgValue = ""
                    break
                default:
                    orgAplicValue = ""
                    break
            }

}
} // cierro el IF COPREC

//Actualizo los campos
put("/rest/api/2/issue/${issueKey}") //
    .queryString("overrideScreenSecurity", Boolean.TRUE)
    .header("Content-Type", "application/json")
    .body([
    fields:[
        (orgAplicCf): orgAplicValue,
        (dirOmicCf): dirOmicValue,
        (contactoOrgCf): contactoOrgValue,
        (mailOrgCf): mailOrgValue,
        (telOrgCf): telOrgValue,
    ]
]).asString()


******************************************************************************************
VALIDACION - USAR JQL EN LA VALIDACION
******************************************************************************************

import com.atlassian.jira.bc.issue.search.SearchService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.web.bean.PagerFilter

def jql = "project = ACDCAM AND priority = Minor AND "Fecha calendarizada" = 2018-12-12 AND Macrosegmento="Banca Personal""
def issuesFromJQL = getIssuesFromJql(jql)

def getIssuesFromJql(String jql) {
    def adminUser = ComponentAccessor.getUserManager().getUserByKey("ncarrivale") //todo this should be an admin user
    def searchService = ComponentAccessor.getComponent(SearchService)
    def issueManager = ComponentAccessor.getIssueManager()

    SearchService.ParseResult parseResult =  searchService.parseQuery(adminUser, jql)
    if (parseResult.isValid()) {
        def searchResult = searchService.search(adminUser, parseResult.getQuery(), PagerFilter.getUnlimitedFilter())
        return searchResult.issues.collect { issueManager.getIssueObject(it.id) }
    }
}
}


******************************************************************************************
VALIDACION - CAMPOS NO NULOS
******************************************************************************************
if ( cfValues['Geografica'] || cfValues['Segmento'] || cfValues['Productos'] || cfValues['Caracteristicas personales'] || cfValues['Actividad-rubro'] || cfValues['Otros'])
{return true} else {return false}


******************************************************************************************
VALIDACION - USANDO JQL EN VALIDACION. Si ya hay un al menos un issue en esa jql, no pasa
******************************************************************************************

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.query.Query;
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField

SearchResults getIssuesByJQL(String jql) throws Exception
    {
        SearchService serchService = ComponentAccessor.getComponentOfType(SearchService.class);
   
      SearchService.ParseResult parseResult = serchService.parseQuery(ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser(),
 jql);
         
        if (!parseResult.isValid())
        {
            throw new Exception("jql is not valid. jql was "+jql);
        }
         
        Query query = parseResult.getQuery();
  
          SearchResults sr = serchService.search(ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser(), query, PagerFilter.getUnlimitedFilter());
            return sr;
    }
 
def customFieldManager = ComponentAccessor.getCustomFieldManager()

def fecha = customFieldManager.getCustomFieldObject("customfield_10506")
def fechavalor = issue.getCustomFieldValue(fecha).clearTime().format("yyyy/MM/dd")
def macro = customFieldManager.getCustomFieldObject("customfield_10518")
def macrovalor = issue.getCustomFieldValue(macro)  
def priorvalor = issue.getPriorityObject().getName()

def consulta= "project = ACDCAM AND priority = "+priorvalor+" AND Macrosegmento= \"Banca Personal\" AND \"Fecha calendarizada\" = \""+fechavalor+"\"" 

def cuenta = getIssuesByJQL(consulta).getTotal()

if (cuenta > 1) {
return false
} else {
    return true
} 


******************************************************************************************
VALIDACION - USER IS IN GROUP
******************************************************************************************

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.Issue
 
def groupManager = ComponentAccessor.getGroupManager()
def customFieldManager = ComponentAccessor.getCustomFieldManager()
def currentUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
def user = currentUser.getName()
 
assert issue instanceof Issue
 
//def cf = customFieldManager.getCustomFieldObjects(issue).find { it.name == "Name of your field here, not ID" }
//def cfValue = issue.getCustomFieldValue(cf)
//log.debug("cfValue: $cfValue")
if (groupManager.isUserInGroup(user, "jira-soporte") || groupManager.isUserInGroup(user, "ACD-campanias")){
return true
}else {return false}


************************************************************
VALIDACION COMPUESTA EN MAS DE UN GRUPO Y TOMAR ISSUE PADRE
************************************************************
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.Issue
 
def groupManager = ComponentAccessor.getGroupManager()
def customFieldManager = ComponentAccessor.getCustomFieldManager()
def im = ComponentAccessor.getIssueManager()
def issue = im.getIssueObject("ACDCAM-14")
def padre = issue.getParentObject()
//def padre = issue.getParentObject()?.getKey() si quiero la key o algo se puede agregar as�

//TOMO USER LOGGEADO
def currentUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
def user = currentUser.getName()

//CAPTURO FECHAS
def fechav = customFieldManager.getCustomFieldObjects(issue).find { it.name == "Fecha de vencimiento" }
def fechavalor = issue.getCustomFieldValue(fechav).clearTime().format("yyyy/MM/dd")
def fechac = customFieldManager.getCustomFieldObjects(issue).find { it.name == "Fecha calendarizada" }
def fechacvalor = padre.getCustomFieldValue(fechac).clearTime().format("yyyy/MM/dd")*/

//VALIDACION COMPUESTA
if (groupManager.isUserInGroup(user, "jira-soporte") || groupManager.isUserInGroup(user, "ACD-campanias")){
    if (fechavalor <= fechacvalor){ return true}else{return false}}
else {return false} 
if (fechavalor <= fechacvalor){ return "OK"}else{return "NO OK"}


************************************************************
CAMBIAR O CALCULAR FECHAS
************************************************************

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.ModifiedValue
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.ComponentManager

 
def groupManager = ComponentAccessor.getGroupManager()
def customFieldManager = ComponentAccessor.getCustomFieldManager()
def im = ComponentAccessor.getIssueManager()
def issue = im.getIssueObject("ACDCAM-21")

//CAPTURO FECHA
def fechac = customFieldManager.getCustomFieldObjects(issue).find { it.name == "Fecha calendarizada" }

//RESTO 12 DIAS Y LO FORMATEO
def fechacvalor = issue.getCustomFieldValue(fechac).minus(10)

//SE LO PEGO A OTRO CAMPO YA EXISTENTE
def vbrief = customFieldManager.getCustomFieldObjects(issue).find { it.name == "Fecha vencimiento de brief" }
if (issue.getCustomFieldValue(vbrief)){
    vbriefvalue = issue.getCustomFieldValue(vbrief)
} else vbriefvalue = 0

vbrief.updateValue(null, issue, new ModifiedValue(vbriefvalue, fechacvalor), null);

return vbrief


import com.atlassian.jira.component.ComponentAccessor

// Get the Project Manager
def projectManager = ComponentAccessor.getProjectManager()

// Iterate over each Project Object
def project = projectManager.getProjectObjects()
def keys

for ( item in project) {
   keys= project.key
}
return keys

import com.atlassian.jira.component.ComponentAccessor

// Get the Project Manager
def projectManager = ComponentAccessor.getProjectManager()

// Iterate over each Project Object
def projects = projectManager.getProjectObjects()
def keyold
def keynew
def result=""


for ( item in projects) {
  keyold= item.getOriginalKey()
  keynew= item.key
  if(keyold!=keynew){
	result= keynew+' key orig: '+keyold+" "+result
      } 
}

//def result = keys[2].getClass()
//def result2= ids[2].getClass()

return result


-----------

import com.atlassian.jira.project.Project
import com.atlassian.jira.project.ProjectManager
import com.atlassian.jira.ComponentManager
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.project.ProjectManager
import com.atlassian.jira.security.roles.*
import com.atlassian.jira.user.ApplicationUsers        
import java.util.Collections

Project myProject = issue.getProjectObject()
def leads 
def projectName, projectKey
 
leads = myProject.getProjectLead()
projectName = issue.getProjectObject().getName();
projectKey = issue.getProjectObject().getKey();
return "projectName is " + projectName + ", Project Key is " + projectKey+ ", Leads are " + leads




def str = 'scp /opt/atlassian/application-data/jira/data/attachments/'+keyproj+'/'+idissue+'/'+attachid+' ftp_scp@sftp-5:"H:'+"\\"+'ftproot'+"\\"+'core_t2_at24'+"\\"+nombre


def str = 'scp /opt/atlassian/application-data/jira/data/attachments/'+keyproj+'/'+idissue+'/'+attachid+' ftp_scp@sftp-5:"H:'+"\\"+'ftproot'+"\\"+'core_t2_at24'+"\\"+nombre+' && ssh usuario@equipo touch directorio_del_scp/nombredelarchivo.OK'

--------
def issueKey = issue.key
// get custom fields
def customFields = get("/rest/api/2/field")
    .asObject(List)
    .body
    .findAll { (it as Map).custom } as List<Map>

// Tomo el issue
def issue = get("/rest/api/2/issue/${issueKey}") //${issueKey}
    .header('Content-Type', 'application/json')
    .asObject(Map)
    .body

//Obtengo todos los ids de los campos
def orgAplicCf = customFields.find { it.name == 'Organismo de aplicacion' }?.id
def dirOmicCf = customFields.find { it.name == 'Direccion OMIC' }?.id
def contactoOrgCf = customFields.find { it.name == 'Contacto Org.' }?.id
def mailOrgCf = customFields.find { it.name == 'Email Org.' }?.id
def telOrgCf = customFields.find { it.name == 'Telefono Org.' }?.id
def CoprecCf = customFields.find { it.name == 'COPREC' }?.id

//TRAE TODOS LOS PROJECTS, DEVUELVE JSON
//URLBASE="https://sgi.bancocredicoop.coop/"
URLP = "rest/api/2/project/"

def jsonprojects = get(URLP)
    .header('Content-Type', 'application/json')
    .asObject(Map)
    .body

def jsonproject_x

$(jQuery.parseJSON(JSON.stringify(jsonprojects))).each(function() {  
         var keyproject = this.id
    	 jsonproject_x = get(URLP+KEYPROJECT+"?expand=projectKeys")
		    .header('Content-Type', 'application/json')
		    .asObject(Map)
		    .body
		 jsonproject_x.projectKeys
});

	//KEYPROJECT = PROYECTO.ID
	//TRAE UN SOLO PROJECT
	//URL1P="https://sgi.bancocredicoop.coop/rest/api/2/project/"+KEYPROJECT+"?expand=projectKeys";
	//JASONPROYECTO_X
	KEYS = JASONPROYECTO_X.projectKeys
	IF KEYS.CANTIDAD > 1
		A ESTE PROYECTO SE LE CAMBIO LA CLAVE EN ALGUN MOMENTO


}










$(jQuery.parseJSON(JSON.stringify(projectos))).each(function() {  
         var ID = this.id;
         var CLASS = this.class;
});



//TRAE UN SOLO PROJECT
URL1P="https://sgi.bancocredicoop.coop/rest/api/2/project/"+projectKey+"?expand=projectKeys";


projectos = 

function hasPks(json1p, key){
	$(jQuery.parseJSON(JSON.stringify(json1p))).each(function() {  
         var keyproject = this.id
    	 }
         
         
************************************************************
19/2/2019 - CREDI
ENVIAR UN MAIL CUSTOM DESDE UNA POSTFUNCION
************************************************************         
         
         
import com.atlassian.mail.Email
import com.atlassian.mail.server.MailServerManager
import com.atlassian.mail.server.SMTPMailServer
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.MutableIssue

/////////////////PARA EJECUTAR DESDE CONSOLA - OBTENER EL ISSUE. SINO COMENTAR///////////////////////////
def issueManager = ComponentAccessor.getIssueManager()
def currentUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
MutableIssue issue = issueManager.getIssueObject("ACDCAM-62")
////////////////////////////////////////////////////////////

def customFieldManager = ComponentAccessor.getCustomFieldManager()
def macro = customFieldManager.getCustomFieldObject("customfield_10600")
def valor = issue.getCustomFieldValue(macro)  

def baseurl = com.atlassian.jira.component.ComponentAccessor.getApplicationProperties().getString("jira.baseurl")

def subject = "[JIRA] - " +issue + " Requiere su revisión"
def body 

body = "<!DOCTYPE html><html><head></head><body>" 
body = body + "<p><b>Issue: </b>"
body = body + "<a href=" + baseurl + "/browse/" + issue.key +">" +issue+"</a>"
body = body + "<br><br>"
body = body + "<b>Tipo: </b>" +issue.issueTypeObject.name + "<br><br>"
body = body + "<b>Resumen: </b>" +issue.summary +"<br><br>"
body = body + "<b>Descripción: </b>" + issue.description + "<br><br>"
body = body + "<b>Informador: </b>" + issue.reporter.displayName+ "<br><br>"
body = body + "<b>Prioridad: </b>" + issue.priority.name  + "<br><br>"
body = body + "</p>"
body = body + "</body></html>"

def emailAddr = ""

switch(valor) { 
   case "Banca personal - macrosegmento": 
		emailAddr= "gebapecomuni@bancocredicoop.coop"
   		break
   case "Banca empresa - macrosegmento": 
		emailAddr= "MBuscaglia@bancocredicoop.coop"
   		break
   case "ECS - macrosegmento": 
		emailAddr= "MBuscaglia@bancocredicoop.coop"
   		break
   default:
		emailAddr= "MBuscaglia@bancocredicoop.coop"
} 

//emailAddr = "ncarrivale@bancocredicoop.coop"

    
def sendEmail(emailAddr, subject, body)
{
def mailServer = ComponentAccessor.getMailServerManager().getDefaultSMTPMailServer()
   
    if (mailServer) 
    {
        Email email = new Email(emailAddr);
        email.setMimeType("text/html")
        email.setSubject(subject);
        email.setBody(body);
        mailServer.send(email);
	}
    
    else
    {
        // Problem getting the mail server from JIRA configuration, log this error
        log.error("Problem getting the mail server from JIRA configuration")
    }
}

sendEmail (emailAddr, subject, body) 



**********************************************
VALIDAR QUE ESTADO ANTERIOR SEA X
**********************************************
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.ComponentManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.comments.CommentManager

def issueManager = ComponentAccessor.getIssueManager()

MutableIssue issue = issueManager.getIssueObject("INI-59")
def ida = issue.getKey()

//Gets List of all changes to 'status' and takes from last one in the list
def changeItem = ComponentAccessor.getChangeHistoryManager().getChangeItemsForField(issue, 'status')?.last()
//variable 'from' is the previous state, 'String' for String type
def estadoant = changeItem?.fromString

if (estadoant=="Ingresada"){
    return false
} else {return true}


**********************************************
VALIDACION CON SWITCH CASE
**********************************************

switch(cfValues['Naturaleza']) { 
   case 'Auditoria': 
    if (cfValues['Nro de Observación']!= null && cfValues['Riesgo Auditoría']!= null){
    	return true    
    }else{return false}
   case 'Normativo': 
    if (cfValues['Organismo']!= null && cfValues['N° de comunicación - Memo - N° Obs-Circ-Ley']!= null){
	return true        
    }else{return false}
    default:
   return true
  } 
  


************************************************************
CAMBIAR RESOLUCION (if resolution is not set or blank)
************************************************************
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.config.ResolutionManager

def resolutionManager = ComponentAccessor.getComponent(ResolutionManager)

if (! issue.getResolutionObject() ) {
    issue.setResolution(resolutionManager.getResolutionByName("Done"))
}

 if (issue.priority?.name == 'Trivial') {
return false
}else {return true}



************************************************************
DISPARAR EVENTO POR SCRIPT
************************************************************


import com.atlassian.jira.event.issue.IssueEventBundle
import com.atlassian.jira.event.issue.IssueEventManager
import com.atlassian.jira.event.issue.IssueEventBundleFactory
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.issue.IssueEvent

// EVENTO 10200 ES NOTIFICAR SUPERVISORES DE PROCESAMIENTO 
Long EVENT_ID = new Long("10200")
def issueManager = ComponentAccessor.getIssueManager()
def currentUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
def user = currentUser.getName()
//def user = ComponentAccessor.getUserManager().getUserByName('user_worker')
 
IssueEventManager issueEventM = ComponentAccessor.getIssueEventManager()
IssueEventBundleFactory issueEventFactory = (IssueEventBundleFactory) ComponentAccessor.getComponent(IssueEventBundleFactory.class)
 
IssueEventBundle eventBundle = issueEventFactory.wrapInBundle(new IssueEvent (issue, null, user, EVENT_ID, true))
issueEventM.dispatchEvent(eventBundle)

cfValues['Macrosegmento'] == 'Banca personal - macrosegmento'


*****************************************************************************
COPIAR ARCHIVOS DESDE SO CON KEY VIEJA, CERTIFICAR COPIA Y COMENTAR RESULTADO
*****************************************************************************

import com.atlassian.jira.ComponentManager
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.comments.CommentManager
import com.atlassian.jira.issue.attachment.Attachment
import org.apache.log4j.Category
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.config.ResolutionManager

compManager = ComponentManager.getInstance()
def resolutionManager = ComponentAccessor.getComponent(ResolutionManager)
// Traigo el nombre de user
def currentUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
def user = currentUser.getName()

// Traigo objetos y luego su key (caso y del proyecto)
MutableIssue issue = ComponentAccessor.getIssueManager().getIssueObject('CRE-18')
def idissue = issue.getKey().replace("CRE", "CXXI")
//def idissue = 'CXXI-15'

//def proj= issue.getProjectObject() 
//def keyproj = proj.getKey()
def keyproj = 'CXXI'

// traigo Manejador de adjuntos
def attachmentManager = ComponentAccessor.getAttachmentManager()

// Traigo lista de adjuntos
def attachlist = attachmentManager.getAttachments(issue)

// Tomo el adjunto en primer lugar y su nombre 
def attachid= attachlist[0].getId()
def nombre= attachmentManager.getAttachment(attachid).getFilename()

// Llamada al SO para la copia
def sout = new StringBuilder(), serr = new StringBuilder()
def sout2 = new StringBuilder(), serr2 = new StringBuilder()

// Comandos
def str = 'scp /opt/atlassian/application-data/jira/data/attachments/'+keyproj+'/'+idissue+'/'+attachid+' prueba@mgrasso:/tmp/nada/'+nombre
def str2 = 'ssh prueba@mgrasso ls -l /tmp/nada/'+nombre 

// Exe1
def proc = str.execute()
proc.consumeProcessOutput(sout, serr)

//Logueo
log.error sout
log.error serr
proc.waitForOrKill(1000) 

// Exe2
def proc2 = str2.execute()
proc2.consumeProcessOutput(sout2, serr2)
proc2.waitForOrKill(1000) 

// Valido si copió OK
if (sout2.contains(nombre)){
    def resp ='TRANSMISIÓN REALIZADA CON EXITO, nombre de archivo: '+nombre
    CommentManager commentMgr = compManager.getCommentManager()
commentMgr = (CommentManager) compManager.getComponentInstanceOfType(CommentManager.class)
commentMgr.create(issue, user, resp , false)
    return "OK"
} else {
    def resp ='TRANSMISIÓN FALLIDA, nombre de archivo: '+nombre
    CommentManager commentMgr = compManager.getCommentManager()
commentMgr = (CommentManager) compManager.getComponentInstanceOfType(CommentManager.class)
commentMgr.create(issue, user, resp , false)
issue.setResolutionId("2")
 //issue.setResolution(resolutionManager.getResolutionByName("Done"))
    return "MAL"
}


************************************************************
TEMPLATE GENGINE PARA MAIL, CON REEMPLAZO DE PRIORIDAD
************************************************************

<p>
<b>Issue:</b> <a href="<% out<<baseUrl%>/browse/<% out << issue.key %>">$issue</a><br><br>
<b>Tipo:</b> <% out << issue.issueTypeObject.name %><br><br>
<b>Resumen:</b> <% out << issue.summary %><br><br>
<b>Descripción:</b> <% out << issue.description %> <br><br>
<b>Informador:</b> <% out << issue.reporter.displayName %> <br><br>
<b>Prioridad:</b> <% if (issue.priority?.name == 'Trivial')
    out << "Baja" %>
    <% if (issue.priority?.name == 'Minor')
    out << "Media" %>
    <% if (issue.priority?.name == 'Major')
    out << "Alta" %>
    <% if (issue.priority?.name == 'Critical')
    out << "Urgente" %>
     <br><br>
<b>Tipo de Tarea:</b> <% out << issue.getCustomFieldValue(componentManager.getCustomFieldManager().getCustomFieldObjectByName("Tipo de Tarea - TES")) %><br><br>
<b>Aplicacion: </b> <% out << issue.getCustomFieldValue(componentManager.getCustomFieldManager().getCustomFieldObjectByName("Aplicación SGT")) %> <br><br>
<b>Entorno:</b> <% out << issue.getCustomFieldValue(componentManager.getCustomFieldManager().getCustomFieldObjectByName("Entorno SGT")) %> <br><br>
<b>Fecha requerida:</b> <% out << issue.getCustomFieldValue(componentManager.getCustomFieldManager().getCustomFieldObjectByName("Fecha Requerida")).clearTime().format("yyyy-MM-dd") %><br><br>
</p>   


************************************************************
VALIDACION - SI SOS TAL USUARIO
************************************************************
import com.atlassian.jira.component.ComponentAccessor

// Get the current user
def currentUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()

if (currentUser.name == "santoniewicz"){
        return true
    }else{
        return false
    }


************************************************************
CANTIDAD DE VECES QUE UN ISSUE PASO POR UN ESTADO
************************************************************

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.MutableIssue

// Traigo objetos y luego su key (caso y del proyecto)
MutableIssue issue = ComponentAccessor.getIssueManager().getIssueObject('SJ-66')

def changeItems = ComponentAccessor.changeHistoryManager.getAllChangeItems(issue)
return changeItems?.findAll{it.field == 'status' && it.getTos().values().contains('In Progress')}.size()


************************************************************
HIPO
Condicion basada en estado, rol, prioridad, issue type
condition based on status, role, priority, issue type
************************************************************

import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.ModifiedValue
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.ComponentManager
import com.atlassian.jira.project.Project
import com.atlassian.jira.project.ProjectManager
import com.atlassian.jira.security.roles.ProjectRole
import com.atlassian.jira.security.roles.ProjectRoleManager

/////////////////PARA EJECUTAR DESDE CONSOLA - OBTENER EL ISSUE. SINO COMENTAR//////////////////////////
//def issueManager = ComponentAccessor.getIssueManager()
//MutableIssue issue = issueManager.getIssueObject("TPD-140")
////////////////////////////////////////////////////////////

def condition = false
def currentUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()

//Consulto sobre roles
ProjectRoleManager projectRoleManager = ComponentManager.getComponentInstanceOfType(ProjectRoleManager.class) as ProjectRoleManager
def isAdmin = projectRoleManager.isUserInProjectRole(currentUser, projectRoleManager.getProjectRole("Administrators"), issue.getProjectObject())
def isUsuarioFinal = projectRoleManager.isUserInProjectRole(currentUser, projectRoleManager.getProjectRole("Usuario Final"), issue.getProjectObject())
def isJefe = projectRoleManager.isUserInProjectRole(currentUser, projectRoleManager.getProjectRole("Jefes"), issue.getProjectObject())
def isQA= projectRoleManager.isUserInProjectRole(currentUser, projectRoleManager.getProjectRole("QA"), issue.getProjectObject())

//Recupero prioridad del issue
def priority = issue.priority.name

//Recupero issueType
def issueType = issue.issueType.name

//Recupero el estado actual
def currState = issue.getStatus().name

//Dependiendo el estado, valido las diferentes condiciones
switch(currState){
	case "En Certificacion": //circuito normal
    	if (isAdmin || isUsuarioFinal)
    			condition = true
        break
	case "En Desarrollo": //circuito corto
    	if (
            (isAdmin || isUsuarioFinal || isJefe) && 
            (priority == "Bloqueante") && 
            (issueType == "Incident")
        	)
    			condition = true
        break
	case "EN PI": //circuito corto
    	if (
            (
				(isAdmin || isUsuarioFinal) && 
            	(priority == "Critical") && 
                (issueType == "Incident")
            )
            ||
            (
				(isAdmin || isQA) && 
            	(priority == "Critical") 
            )
           )
    			condition = true
        break
    default:
        break
}

// https://jamieechlin.atlassian.net/wiki/spaces/GRV/pages/1212424/Conditions
passesCondition = condition


************************************************************
HIPO - 15/3/2019
Postfuncion que crea issues
Post function create issues
************************************************************

import com.atlassian.jira.bc.issue.IssueService
import com.atlassian.jira.component.ComponentAccessor

import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.IssueInputParameters
import com.atlassian.jira.issue.link.IssueLinkManager

import com.atlassian.jira.user.ApplicationUser


/////////////////PARA EJECUTAR DESDE CONSOLA - OBTENER EL ISSUE. SINO COMENTAR//////////////////////////
def issueManager = ComponentAccessor.getIssueManager()
MutableIssue issue = issueManager.getIssueObject("TPD-156")
////////////////////////////////////////////////////////////


// Creo la funcion que crea una subtarea
def crearTarea(MutableIssue issue, String summary, String description)
{
    def taskIssueTypeId = "10101"
    
    IssueService issueService = ComponentAccessor.getIssueService()
	IssueInputParameters issueInputParameters = issueService.newIssueInputParameters();
    
     /*New Issue Data*/
    issueInputParameters.setProjectId( issue.getProjectObject().getId() );
    issueInputParameters.setSummary( summary );
    issueInputParameters.setDescription(description );
    issueInputParameters.setIssueTypeId(taskIssueTypeId);
    if (issue.priority!=null)
        issueInputParameters.setPriorityId(issue.priority.id) // "5" => "Lowest" x ej
    issueInputParameters.setReporterId(issue.reporterId)
    issueInputParameters.setAssigneeId(issue.assigneeId)

    ApplicationUser user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()

    IssueService.CreateValidationResult createValidationResult   
    createValidationResult = issueService.validateCreate(user, issueInputParameters)


    if (createValidationResult.isValid())
    {
        IssueService.IssueResult createResult = issueService.create(user, createValidationResult)

        if (createResult.isValid())
        {
           //Se linkea el issue creado con el actual
           def newIssue = createResult.getIssue()
           IssueLinkManager linkMgr = ComponentAccessor.getIssueLinkManager()
           Long sourceIssueId = new Long (newIssue.id)
           Long destinationIssueId = new Long (issue.id)
	       Long issueLinkTypeId = new Long(10003) // Incidencias > Vinculacion de incidencias "10003" => "Relates" x ej
           Long sequence = new Long (1)// In which order the link will appear in the UI
           linkMgr.createIssueLink(sourceIssueId, destinationIssueId, issueLinkTypeId, sequence, user)
        }else
        {
            log.debug("Error while creating the issue.")
            log.debug("Errors:  " + createValidationResult.getErrorCollection())
            return "ERROR1 - See log"
        }
    }
    else
    {
        log.error("Error: createValidationResult is FALSE")
        log.error("Errors:  " + createValidationResult.getErrorCollection())
        return "ERROR2 - See log"
    }
}
                                                 
                                                 
                                                
//Recupero el estado actual
def currState = issue.getStatus().name
def summaryNormAmbiente = "Normalizacion de Ambiente"
def summaryPedidoMerge = "[Pedido de Merge]"

def descriptionNormAmbiente = "Se solicita la normalizacion del ambiente de referencia debido a cambios realizados por el issue "+ issue.key 
def descriptionPedidoMerge = "Se solicita la revisión de posibles merge debido a cambios realizados por el issue " + issue.key


//Si es circuito corto, por cada ambiente que se saltea crear 1 tarea "pedido de merge" y N "normalizar ambiente"
switch(currState){
	case "En Desarrollo": //circuito corto: salteo PI y Homo
		crearTarea ( issue, "[" +summaryNormAmbiente + " PI]",descriptionNormAmbiente)                                     
		crearTarea ( issue, "[" +summaryNormAmbiente + " PI]",descriptionNormAmbiente)                                     
		crearTarea ( issue, summaryPedidoMerge,descriptionPedidoMerge)                                     
	    break
	case "EN PI": //circuito corto: salteo Homo
		crearTarea ( issue, "[" +summaryNormAmbiente + " HOMO]",descriptionNormAmbiente)                                     
    	crearTarea ( issue, summaryPedidoMerge,descriptionPedidoMerge)                                     
    	break
    default:
        break
}

************************************************************
HIPO - 15/3/2019
Obtener todos los usuarios (email adress) de un rol
************************************************************

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.bc.projectroles.ProjectRoleService;
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.util.SimpleErrorCollection


/////////////////PARA EJECUTAR DESDE CONSOLA - OBTENER EL ISSUE. SINO COMENTAR//////////////////////////
def issueManager = ComponentAccessor.getIssueManager()
MutableIssue issue = issueManager.getIssueObject("TPD-156")
///////////////////////////////////////////////////////////

ProjectRoleService projectRoleService = (ProjectRoleService) ComponentAccessor.getComponentOfType(ProjectRoleService.class);
//Get Project role email addresses
def roleName = "Jefes"
def projectRole = projectRoleService.getProjectRoleByName(roleName, new SimpleErrorCollection())
def actors = projectRoleService.getProjectRoleActors(projectRole, issue.projectObject, new SimpleErrorCollection())
def toAddresses = actors?.getApplicationUsers()*.emailAddress.join(",")

return toAddresses

****************************************************************
VALIDACION POR USUARIO EN ROL
****************************************************************

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.ComponentManager
import com.atlassian.jira.project.ProjectManager
import com.atlassian.jira.security.roles.ProjectRoleManager
import com.atlassian.jira.issue.MutableIssue

def currentUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()

//Consulto sobre roles
ProjectRoleManager projectRoleManager = ComponentManager.getComponentInstanceOfType(ProjectRoleManager.class) as ProjectRoleManager
return projectRoleManager.isUserInProjectRole(currentUser, projectRoleManager.getProjectRole("Internal Test"), issue.getProjectObject())


************************************************************
CREDI - 09/4/2019
EJECUTAR COMANDO UNIX PARA COPIAR ARCHIVOS
************************************************************

// Este script copia el único archivo adjunto al issue a un directorio destino.
// Sobre dicho directorio destino existe un proceso que buscar archivos con nombre "X" y realiza alguna acción
// (con ese nombre "X" se deben adjuntar al issue)
// Para evitar que este proceso tome este archivo "X" mientras se está copiando y por ende esté corrupto
// como primer paso el archivo se copia con un nombre ficticio "archivoprevio8.txt"
// Y una vez copiado se renobra al nombre original "X"

import com.atlassian.jira.ComponentManager
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.comments.CommentManager
import com.atlassian.jira.issue.attachment.Attachment
import org.apache.log4j.Category
import com.atlassian.jira.issue.Issue

def keyproj = 'CXXI'

compManager = ComponentManager.getInstance()
// Traigo el nombre de user
def currentUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
def user = currentUser.getName()

// Traigo objetos y luego su key (caso y del proyecto)
//MutableIssue issue = ComponentAccessor.getIssueManager().getIssueObject('CRE-31')
def idissue = issue.getKey().replace("CRE", keyproj)
//def idissue = 'CXXI-31'

// traigo Manejador de adjuntos
def attachmentManager = ComponentAccessor.getAttachmentManager()

// Traigo lista de adjuntos
def attachlist = attachmentManager.getAttachments(issue)

// Tomo el adjunto en primer lugar y su nombre 
def attachid= attachlist[0].getId()
def nombre= attachmentManager.getAttachment(attachid).getFilename()

CommentManager commentMgr = compManager.getCommentManager()
commentMgr = (CommentManager) compManager.getComponentInstanceOfType(CommentManager.class)

//Arma comando para copiar un archivo o directorio del sistema local a un sistema remoto.
def str = 'scp /opt/atlassian/application-data/jira/data/attachments/'+keyproj+'/'+idissue+'/'+attachid+' ftp_scp@sftp-5:'+"/"+'core_t2_at24'+"/"+'archivoprevio8.txt'

String comentario = ""

comentario = comentario + commentAndLog ('--- INICIO PROCESO OCASIONALES CRECER --- '+ idissue, "black")
comentario = comentario + commentAndLog ('Paso 1. STR: '+str ,"black")
         
// Llamada al SO para la copia
def proc = str.execute()
proc.waitFor()

int res = proc.exitValue()
comentario = comentario + commentAndLog ('Paso 2. proc.exitValue(): '+res , "black")

if (res != 0) {
    comentario = comentario + commentAndLog ('Paso 2. proc.err.text: '+proc.err.text , "red")
}
else
{
    comentario = comentario + commentAndLog ('Paso 2. TRANSMISIÓN REALIZADA CON EXITO', "green")
    def str2 = 'ssh ftp_scp@sftp-5 mv H:'+'\\'+'ftproot'+'\\'+'core_t2_at24'+'\\'+'archivoprevio8.txt '+'H:'+'\\'+'ftproot'+'\\'+'core_t2_at24'+'\\'+nombre

    comentario = comentario + commentAndLog ('Paso 3. str2: '+str2,  "black")
    def proc2 = str2.execute()
    proc2.waitFor()

    int res2 = proc2.exitValue()
    comentario = comentario + commentAndLog ('Paso 4. proc2.exitValue(): '+res2 ,  "black")
	if (res2 != 0) {
        comentario = comentario + commentAndLog ('Paso 4. proc2.err.text: '+proc2.err.text ,  "red")
	}
    else
    {
        comentario = comentario + commentAndLog ('Paso 4. RENOMBRO EL ARCHIVO CON EXITO' ,  "green")
    }
}    

comentario = comentario + commentAndLog ('--- FIN PROCESO OCASIONALES CRECER ---', "black")
commentMgr.create(issue, user, comentario , false)

def commentAndLog(String info, String color)
{
	log.warn(info)
    return "{color:" +color+ "}" + info +  "{color}" + "\n"
}
