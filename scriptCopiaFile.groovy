/******************************************************************************************
LOGUEAR LINEA
******************************************************************************************/

import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.opensymphony.workflow.InvalidInputException
import com.atlassian.jira.issue.attachment.Attachment

import com.atlassian.jira.component.ComponentAccessor

def attachmentManager = ComponentAccessor.getAttachmentManager()

//Capturar nombre del adjunto dado por el "key"
def nombre= attachmentManager.getAttachment(12301).getFilename()

def sout = new StringBuilder(), serr = new StringBuilder()
def proc = 'ls -l /opt/atlassian/application-data/jira/data/attachments/SJ/SJ-59'.execute()
proc.consumeProcessOutput(sout, serr)
proc.waitForOrKill(1000)

throw new InvalidInputException(nombre)