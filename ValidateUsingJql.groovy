/******************************************************************************************
VALIDACION - USAR JQL EN LA VALIDACION
******************************************************************************************/

import com.atlassian.jira.bc.issue.search.SearchService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.web.bean.PagerFilter

def jql = "project = ACDCAM AND priority = Minor AND "Fecha calendarizada" = 2018-12-12 AND Macrosegmento="Banca Personal""
def issuesFromJQL = getIssuesFromJql(jql)

def getIssuesFromJql(String jql) {
    def adminUser = ComponentAccessor.getUserManager().getUserByKey("ncarrivale") //todo this should be an admin user
    def searchService = ComponentAccessor.getComponent(SearchService)
    def issueManager = ComponentAccessor.getIssueManager()

    SearchService.ParseResult parseResult =  searchService.parseQuery(adminUser, jql)
    if (parseResult.isValid()) {
        def searchResult = searchService.search(adminUser, parseResult.getQuery(), PagerFilter.getUnlimitedFilter())
        return searchResult.issues.collect { issueManager.getIssueObject(it.id) }
    }
}
}
