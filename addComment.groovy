/******************************************************************************************
COMENTAR UN ISSUE - jira server 6.3.14 - script runner 3.0.10
*******************************************************************************************/

import com.atlassian.jira.ComponentManager
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.comments.CommentManager

compManager = ComponentManager.getInstance()

def issueManager = ComponentAccessor.getIssueManager()
//Tomo objeto user actualmente logeado
def currentUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
def user = currentUser.getName()
//Tomo objeto issue 
MutableIssue issue = issueManager.getIssueObject("PHP-16")
def ida = issue.getKey()

CommentManager commentMgr = compManager.getCommentManager()
//Esto ni idea pero lo castea
commentMgr = (CommentManager) compManager.getComponentInstanceOfType(CommentManager.class)
//comento
commentMgr.create(issue, user, ida, false)