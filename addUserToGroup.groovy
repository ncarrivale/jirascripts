/******************************************************************************************
AGREGAR USER A UN GRUPO
******************************************************************************************/

import com.atlassian.jira.component.ComponentAccessor;

def uu = ComponentAccessor.getUserUtil()
def user2 = uu.getUserByName("prueba123")
def group = uu.getGroupObject("pruebagroup")
uu.addUserToGroup(group, user2.getDirectoryUser())