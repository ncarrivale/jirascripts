/******************************************************************************************
VALIDACION - USANDO JQL EN VALIDACION. Si ya hay un al menos un issue en esa jql, no pasa
******************************************************************************************/

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.query.Query;
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField

SearchResults getIssuesByJQL(String jql) throws Exception
    {
        SearchService serchService = ComponentAccessor.getComponentOfType(SearchService.class);
   
      SearchService.ParseResult parseResult = serchService.parseQuery(ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser(),
 jql);
         
        if (!parseResult.isValid())
        {
            throw new Exception("jql is not valid. jql was "+jql);
        }
         
        Query query = parseResult.getQuery();
  
          SearchResults sr = serchService.search(ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser(), query, PagerFilter.getUnlimitedFilter());
            return sr;
    }
 
def customFieldManager = ComponentAccessor.getCustomFieldManager()

def fecha = customFieldManager.getCustomFieldObject("customfield_10506")
def fechavalor = issue.getCustomFieldValue(fecha).clearTime().format("yyyy/MM/dd")
def macro = customFieldManager.getCustomFieldObject("customfield_10518")
def macrovalor = issue.getCustomFieldValue(macro)  
def priorvalor = issue.getPriorityObject().getName()

def consulta= "project = ACDCAM AND priority = "+priorvalor+" AND Macrosegmento= \"Banca Personal\" AND \"Fecha calendarizada\" = \""+fechavalor+"\"" 

def cuenta = getIssuesByJQL(consulta).getTotal()

if (cuenta > 1) {
return false
} else {
    return true
} 