
/******************************************************************************************
COMPROBAR SI UN USER ESTA EN UN GRUPO
******************************************************************************************/

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.ComponentManager
import com.atlassian.jira.security.groups.GroupManager

def currentUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()

GroupManager groupManager = ComponentManager.getComponentInstanceOfType(GroupManager.class)

if ( groupManager.isUserInGroup(currentUser.name, "jira-soporte")) {
	return true
}